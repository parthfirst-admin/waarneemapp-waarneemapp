import Vue from 'vue'

export const state = () => ({
    shifts: [
        {
            title: 'Title 1',
            description: 'Descirption 1',
            dateRange: [new Date('Thu Nov 25 2021 00:00:00 GMT+0530 (India Standard Time)'), new Date('Thu Nov 26 2021 00:00:00 GMT+0530 (India Standard Time)'), new Date('Thu Nov 27 2021 00:00:00 GMT+0530 (India Standard Time)')],
            dates: [
                {
                    date: new Date('Thu Nov 25 2021 00:00:00 GMT+0530 (India Standard Time)'),
                    startTime: new Date('Thu Nov 25 2021 13:00:00 GMT+0530 (India Standard Time)'),
                    endTime: new Date('Thu Nov 25 2021 14:00:00 GMT+0530 (India Standard Time)'),
                    type: 'Consultation',
                    price: '70'
                },
                {
                    date: new Date('Thu Nov 26 2021 00:00:00 GMT+0530 (India Standard Time)'),
                    startTime: new Date('Thu Nov 26 2021 13:00:00 GMT+0530 (India Standard Time)'),
                    endTime: new Date('Thu Nov 26 2021 14:00:00 GMT+0530 (India Standard Time)'),
                    type: 'Telephone',
                    price: '50'
                },
                {
                    date: new Date('Thu Nov 27 2021 00:00:00 GMT+0530 (India Standard Time)'),
                    startTime: new Date('Thu Nov 27 2021 13:00:00 GMT+0530 (India Standard Time)'),
                    endTime: new Date('Thu Nov 27 2021 14:00:00 GMT+0530 (India Standard Time)'),
                    type: 'Ambulance',
                    price: '40'
                }
            ]
        },
        {
            title: 'Title 2',
            description: 'Descirption 2',
            dateRange: [new Date('Thu Nov 27 2021 00:00:00 GMT+0530 (India Standard Time)'), new Date('Thu Nov 27 2021 00:00:00 GMT+0530 (India Standard Time)')],
            dates: [
                {
                    date: new Date('Thu Nov 27 2021 00:00:00 GMT+0530 (India Standard Time)'),
                    startTime: new Date('Thu Nov 27 2021 13:00:00 GMT+0530 (India Standard Time)'),
                    endTime: new Date('Thu Nov 27 2021 14:00:00 GMT+0530 (India Standard Time)'),
                    type: 'Consultation',
                    price: '70'
                },
            ]
        }
    ],
    filter: {
        price: [],
    }
})

export const mutations = {
    addShift(state, data) {
        state.shifts.push(data)
    },
    editShift(state, data){
        // Update data into particular position of state data
        Vue.set(state.shifts, data.position, data.data)
    },
    deleteShift(state, data){
        state.shifts.splice(data, 1);
    },
    setFilterPrice(state, data){
        state.filter.price = data
    },
}

export const getters = {
    getPriceRange(state){
        if(state.shifts.length > 0){
            let mapValue = state.shifts.map(({dates}) => dates.map(({price}) => parseInt(price)))
            let min = Math.min(...[].concat(...mapValue))
            let max = Math.max(...[].concat(...mapValue))
            return {min: min, max: max}
        }
        return {min: 0, max: 200} //Default Return
    },
    getFilterPrice(state){
        return state.filter.price
    }
}