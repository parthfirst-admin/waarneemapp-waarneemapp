import { format } from "date-fns"

const dateDisplay = (date) => {
    return format(new Date(date), 'dd-MM-yyyy')
}

const timeDisplay = (time) => {
    return format(new Date(time), 'hh:mm a')
}

export {
    dateDisplay,
    timeDisplay
}