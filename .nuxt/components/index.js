export { default as Heading } from '../../components/heading.vue'
export { default as PriceFilter } from '../../components/priceFilter.vue'
export { default as ShiftCard } from '../../components/shiftCard.vue'
export { default as ShiftDates } from '../../components/shiftDates.vue'
export { default as SideBar } from '../../components/sideBar.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
